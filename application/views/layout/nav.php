<?php
// AMBIL DATA MENU DARI KONFIGURASI
$nav_produk = $this->konfigurasi_model->nav_produk();
?>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.html"><img src="<?php echo base_url('assets/upload/image/' . $site->logo) ?>" alt="<?php echo $site->namaweb ?> | <?php echo $site->tagline ?>"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <!-- HOME -->
                <li class="nav-item"><a href="<?php echo base_url() ?>" class="nav-link"><strong>Beranda</strong></a></li>
                <!-- MENU PRODUK -->

                <ul class="navbar-nav dropdown list-unstyled">
                    <li class="nav-item"><a href="<?php echo base_url('produk') ?>" class="nav-link dropdown-toggle"><strong>Produk</strong></a></li>
                    <ul class="dropdown-menu">
                        <?php foreach ($nav_produk as $nav_produk) { ?>
                            <li><a class="dropdown-item" href="<?= base_url('produk/kategori/' . $nav_produk->slug_kategori) ?>"><?= $nav_produk->nama_kategori; ?></a></li>
                        <?php } ?>
                        <li class="divider"></li>
                    </ul>
                </ul>
                <li class="nav-item"><a href="menu.html" class="nav-link"><strong>Catalogue</strong></a></li>
                <li class="nav-item"><a href="about.html" class="nav-link"><strong>Kontak Kami</strong></a></li>
                <li class="nav-item cta"><a href="reservation.html" class="nav-link">Book a table</a></li>


                <!-- Nav Item - Alerts -->
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cart-plus fa-fw"></i>
                        <!-- Counter - Alerts -->
                        <?php
                        // check data belanjaan ada atau tidak
                        $keranjang = $this->cart->contents();

                        ?>
                        <span class="badge badge-danger badge-counter"><?php echo count($keranjang) ?></span>
                    </a>
                    <!-- Dropdown - Alerts -->
                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                        <h6 class="dropdown-header">
                            Keranjang Belanja
                        </h6>
                        <?php
                        if (empty($keranjang)) {
                        ?>
                <li class="header-cart-item">
                    <p class="alert alert-success">Keranjang Kosong</p>
                </li>
                <?php
                        } else {
                            $total = 'Rp. ' . number_format($this->cart->total(), '0', ',', '.');
                            foreach ($keranjang as $keranjang) {
                                $id_produk = $keranjang['id'];
                                // ambil data produk
                                $produknya = $this->produk_model->detail($id_produk);
                ?>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                        <div class="mr-3">
                        </div>
                        <div>
                            <span class="font-weight-bold"><?php echo $keranjang['name'] ?></span>
                            <div class="small text-gray-500"><?php echo $keranjang['qty'] ?> X Rp. <?php echo $keranjang['price'], '0', ',', '.' ?></div>
                        </div>
                    </a>
            <?php
                            } // tutup foreach
                        } // tutup if
            ?>
            <div class="small text-gray-500 text-center">
                <!-- Total : <?php echo $total ?> -->
            </div>
            <a class="dropdown-item text-center small text-red-500" href="<?php echo base_url('belanja/keranjang') ?>">Tampilkan Semua</a>
            <a class="dropdown-item text-center small text-red-500" href="<?php echo base_url('belanja/checkout') ?>">Check Out</a>
        </div>
        </li>
        </ul>


    </div>
    </div>
</nav>
<!-- END nav -->