<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-12 text-center heading-section ftco-animate">
                <span class="subheading">SAE</span>
                <h2 class="mb-4">PRODUK TERBARU</h2>
            </div>
        </div>
        <div class="row">
            <?php foreach ($produk as $produk) : ?>
                <div class="col-md-6 col-lg-3 ftco-animate">
                    <?php
                    // Form untuk memproses belanjaan
                    echo form_open(base_url('belanja/add'));
                    // elemen yang dibawa
                    echo form_hidden('id', $produk->id_produk);
                    echo form_hidden('qty', 1);
                    echo form_hidden('price', $produk->harga);
                    echo form_hidden('name', $produk->nama_produk);
                    // Elemen redirect
                    echo form_hidden('redirect_page', str_replace('index.php', '', current_url()));
                    ?>
                    <div class="staff">
                        <div class="img" style="background-image: url(<?php echo base_url('assets/upload/image/' . $produk->gambar) ?>);"></div>
                        <div class="text pt-4">
                            <h3><?= $produk->nama_produk; ?></h3>
                            <span class="position mb-2">IDR <?= $produk->harga, '0', ',', '.'; ?></span>
                            <div class="faded">
                                <ul class="ftco-social d-flex">
                                    <li class="ftco-animate"><a href="<?php base_url('produk/detail/' . $produk->slug_produk) ?>"><span class="fas fa-search-plus"></span></a></li>
                                    <button type="submit" value="submit" class="ftco-animate"><span class="fas fa-shopping-cart"></span></button>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                    // closing form
                    echo form_close();
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>