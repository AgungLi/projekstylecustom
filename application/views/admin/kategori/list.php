<div class="container-fluid">
    <p>
        <a href="<?php echo base_url('admin/kategori/tambah') ?>" class="btn btn-sm btn-primary mb-3"><i class="fas fa-plus fa-sm"></i>Tambah kategori</a>
    </p>
    <?php
    if ($this->session->flashdata('sukses')) {
        echo '<p class"alert alert-success">';
        echo $this->session->flashdata('sukses');
        echo '</div>';
    }
    ?>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Slug</th>
            <th>Urutan</th>
            <th colspan="4">Aksi</th>
        </tr>

        <?php
        $no = 1;
        foreach ($kategori as $kategori) : ?>
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $kategori->nama_kategori ?></td>
                <td><?php echo $kategori->slug_kategori ?></td>
                <td><?php echo $kategori->urutan ?></td>
                <td>

                </td>
                <td>
                    <div class="btn btn-success btn-sm"><i class="fas fa-search-plus"></i></div>
                </td>
                <td> <a href="<?php echo base_url('admin/kategori/edit/' . $kategori->id_kategori) ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a> </td>

                <td><a href="<?php echo base_url('admin/kategori/delete/' . $kategori->id_kategori) ?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin menghapus?')"><i class="fas fa-trash"></i></a></td>

            </tr>
        <?php endforeach; ?>
    </table>
</div>